package example.rest;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/userResource")
public class UserResourceClient {
	UserResource resource = new UserResource();

	@POST
	@Path("/updateUser/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createUser(@PathParam("param") String user) {
		String output;
		String[] parts = user.split("-");
		if (parts.length < 2)
			return Response.status(200).entity("Please give user as username-password").build();

		User u = new User(parts[0], parts[1]);

		try {
			if (resource.getUser(u.getUsername()) != null)
				return Response.status(200).entity("User already exists").build();
			else
				resource.addUser(u);
		} catch (IOException e) {
			return Response.status(200).entity(e.getMessage()).build();
		}
		output = "New user : " + u;
		return Response.status(200).entity(output).build();
	}

	/**
	 * Updates in userName.txt the password and displays using JSON format the
	 * user and the password or the error message in case of failure. The input
	 * user must contain the username and the password separated by the '-'
	 * character.
	 */
	@POST
	@Path("/updateUser/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUser(@PathParam("param") String user) {
		String output;
		String[] parts = user.split("-");
		if (parts.length < 2)
			return Response.status(200).entity("Please give user as username-password").build();

		User u = new User(parts[0], parts[1]);

		try {
			if (resource.getUser(u.getUsername()) == null)
				return Response.status(200).entity("User not found").build();
			else
				resource.updateUser(u);
		} catch (IOException e) {
			return Response.status(200).entity(e.getMessage()).build();
		}
		output = "New user : " + u;
		return Response.status(200).entity(output).build();
	}

	/**
	 * Removes from the file userName.txt a user and displays using JSON format
	 * a message about deleting it.
	 */
	@GET
	@Path("/deleteUser/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteUser(@PathParam("param") String userName) {
		String output;
		try {
			User u = resource.getUser(userName);
			if (u == null)
				return Response.status(200).entity("User not found").build();
			else
				resource.deleteUser(u);
		} catch (IOException e) {
			return Response.status(200).entity(e.getMessage()).build();
		}
		output = "User deleted";
		return Response.status(200).entity(output).build();
	}

	@GET
	@Path("/getUser/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("param") String userName) {
		User u = resource.getUser(userName);
		if (u == null) {
			return Response.status(200).entity("User not found").build();
		}
		return Response.status(200).entity(u.toString()).build();
	}

	@GET
	@Path("/getAll")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		String output = "";
		for (User u : resource.getAll()) {
			output += u + " ; ";
		}
		if (output.isEmpty())
			output = "No users found";
		return Response.status(200).entity(output).build();
	}

}