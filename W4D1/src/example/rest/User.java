package example.rest;

public class User {
	String username;
	String password;

	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return this.username + " " + this.password;
	}

	@Override
	public boolean equals(Object o) {
		if (((User) o).username.equals(this.username))
			return true;
		return false;
	}
}
