package example.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserResource {
	private List<User> users = new ArrayList<User>();

	public UserResource() {
		User u = new User("a", "a");
		users.add(u);
		u = new User("b", "b");
		users.add(u);
		u = new User("b", "b");
		users.add(u);
	}

	public void addUser(User user) throws IOException {
		users.add(user);

	}

	public void updateUser(User user) throws IOException {
		for (User u : users) {
			if (u.getUsername().equals(user.getUsername())) {
				u.setPassword(user.getPassword());

			}
		}

	}

	public void deleteUser(User user) throws IOException {
		users.remove(user);

	}

	public User getUser(String username) {
		for (User u : users) {
			if (u.getUsername().equals(username)) {
				return u;
			}
		}
		return null;
	}

	public List<User> getAll() {
		return users;
	}

}
