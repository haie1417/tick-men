package example.java.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import example.java.entities.User;

public class UserDaoImpl implements UserDao {
	
	@PersistenceContext
	private EntityManager entityManager;

	public User get(Long byId) {
		return entityManager.find(User.class, byId);
	}

	public List<User> list() {
		return entityManager.createQuery("select p from Person p", User.class).getResultList();
	}

	public void save(User person) {
		entityManager.merge(person);
	}

	public void delete(User person) {
		person = entityManager.find(User.class, person.getId());
		entityManager.remove(person);
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
