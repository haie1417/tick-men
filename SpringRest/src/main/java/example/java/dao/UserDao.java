package example.java.dao;

import java.util.List;

import example.java.entities.User;

public interface UserDao {

	User get(Long byId);
	List<User> list();
	void save(User person);
	void delete(User person);
		
}
