package example.java.services;

import java.util.Date;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import example.java.dao.UserDao;
import example.java.entities.User;


@Service
@Transactional
public class UserServiceImpl implements UserService{
	@Autowired
	private UserDao personDAO;
	
	public User get(Long byId) {
		return personDAO.get(byId);
	}

	public List<User> list() {
		return personDAO.list();
	}

	public void save(User person) throws ValidationException	{
		if (person.getBirthDate().before(getCurrentDate())){
			throw new ValidationException("birth date must be before current date");
		}else{
			personDAO.save(person);
		}
	}

	public void delete(User person) {
		personDAO.delete(person);
	}

	public void setPersonDAO(UserDao personDAO) {
		this.personDAO = personDAO;
	}
	
	private Date getCurrentDate(){
		return new Date();
	}
}
