package example.java.services;

import java.util.List;

import example.java.entities.User;

public interface UserService {
	User get(Long byId);
	List<User> list();
	void save(User person);
	void delete(User person);
}
