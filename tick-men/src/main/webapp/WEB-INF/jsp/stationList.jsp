<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<title>DriveMe</title>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/style.css"/>" rel="stylesheet"
	type="text/css" />

</head>
<body>
	<div id="header">
		<nav>
			<ul class="clearfix">
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/route/list">Routes</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/ticket/list">Tickets</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/station/list">Stations</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/person/list">Users</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/index">Log out</a></li>
			</ul>
		</nav>

	</div>
	<div id="content">

		<table width="360">
			<tr>
				<th>Name</th>
			</tr>
			<c:forEach items="${stations}" var="station">
				<tr>
					<td><c:out value="${station.name}" /></td>
					<td><a href="view/${station.id}">View</a></td>
				</tr>
			</c:forEach>
		</table>

		<br>
		<div id="view">
			<a href="view/0">New</a>
		</div>
	</div>
</body>
</html>