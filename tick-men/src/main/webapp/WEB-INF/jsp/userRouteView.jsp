<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
<title>DriveMe</title>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/style.css"/>" rel="stylesheet"
	type="text/css" />

</head>
<body>
	<div id="header">
		<nav>
			<ul class="clearfix">
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/route/ulist">Routes</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/station/ulist">Stations</a></li>
				<li><a
					href="/tick-men-0.0.1-SNAPSHOT/web/ticket/uview/${person.id}">Profile</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/index">Log out</a></li>
			</ul>
		</nav>

	</div>
	<div id="content">


		<form action="../save" method="post">
			<input type="hidden" name="id" value="${route.id}" />


			<table width="360">
				<tr>
					<td>Name</td>
					<td>${route.name}</td>
				</tr>

				<tr>
					<td>Type</td>
					<td>${route.type}</td>
				</tr>

				<tr>
					<td>Start</td>
					<td>${route.start}</td>
				</tr>

				<tr>
					<td>End</td>
					<td>${route.end}</td>
				</tr>

				<td>Frequency</td>
				<td>${route.frequency}</td>
				</tr>

				<tr>
					<td>Length (km)</td>
					<td>${route.lengthInKms}</td>
				</tr>

				<tr>
					<td>Time (minutes)</td>
					<td>${route.lengthInMins}</td>
				</tr>
			</table>

			<h3>Stops</h3>
			<table width="360">
				<c:forEach items="${route.stations}" var="stations">
					<tr>
						<td><c:out value="${station.name}" /></td>
					</tr>
				</c:forEach>
			</table>

		</form>

		<div id="view">
			<a href="/tick-men-0.0.1-SNAPSHOT/web/route/ulist">Back</a>
		</div>

	</div>
</body>
</html>