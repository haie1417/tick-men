<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<title>DriveMe</title>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/style.css"/>" rel="stylesheet"
	type="text/css" />

</head>
<body>
	<div id="content">

		<div id="welcome">
				<center><h1>Welcome</h1></center>
		</div>

		<form action="/tick-men-0.0.1-SNAPSHOT/web/login" method="post">
			<input type="hidden" name="id" value="${person.id}" />


			<table class="login" width="360"
				style="margin-left: auto; margin-right: auto;">
				<tr>
					<td>Email address</td>
					<td><input type="text" name="email" value="${person.email}"
						required /></td>
				</tr>

				<tr>
					<td>Password</td>
					<td><input type="password" name="password"
						value="${person.password}" required /></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" value="Sign in" /></td>
				</tr>
			</table>
		</form>


		<form action="/tick-men-0.0.1-SNAPSHOT/web/person/save" method="post">
			<input type="hidden" name="id" value="${person.id}" />
			<input type="hidden" name="action" value="signup" />


			<table class="login" width="360"
				style="margin-left: auto; margin-right: auto;">
				<tr>
					<td>First name</td>
					<td><input type="text" name="firstName"
						value="${person.firstName}" required /></td>
				</tr>

				<tr>
					<td>Last name</td>
					<td><input type="text" name="lastName"
						value="${person.lastName}" required /></td>
				</tr>

				<tr>
					<td>CNP</td>
					<td><input type="text" name="cnp" value="${person.cnp}"
						required /></td>
				</tr>

				<tr>
					<td>Type</td>
					<td><select name="personType">
							<option value=0>Employee</option>
							<option value=1>User</option>
					</select></td>
				</tr>

				<tr>
					<td>Email address</td>
					<td><input type="text" name="email" value="${person.email}"
						required /></td>
				</tr>

				<tr>
					<td>Password</td>
					<td><input type="password" name="password"
						value="${person.password}" required /></td>
				</tr>

				<tr>
					<td>Confirm password</td>
					<td><input type="password" name="confirmpassword"
						value="${person.password}" required /></td>
				</tr>

				<tr>
					<td></td>
					<td><input type="submit" value="Sign up" /></td>
				</tr>

			</table>
		</form>
	</div>
</body>
</html>