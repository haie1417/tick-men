<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<title>DriveMe</title>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/style.css"/>" rel="stylesheet"
	type="text/css" />

</head>
<body>
	<div id="header">
		<nav>
			<ul class="clearfix">
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/route/ulist">Routes</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/station/ulist">Stations</a></li>
				<li><a
					href="/tick-men-0.0.1-SNAPSHOT/web/ticket/uview/${person.id}">Profile</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/index">Log out</a></li>
			</ul>
		</nav>

	</div>
	<div id="content">

		<form action="../save" method="post">
			<input type="hidden" name="id" value="${person.id}" /> <input
				type="hidden" name="action" value="signup" />


			<table width="360">
				<tr>
					<td>First name</td>
					<td><input type="text" name="firstName"
						value="${person.firstName}" required /></td>
				</tr>

				<tr>
					<td>Last name</td>
					<td><input type="text" name="lastName"
						value="${person.lastName}" required /></td>
				</tr>

				<tr>
					<td>CNP</td>
					<td><input type="text" name="cnp" value="${person.cnp}"
						required /></td>
				</tr>

				<tr>
					<td>Type</td>
					<td><select name="personType">
							<option value=-1>-- select type --</option>
							<option value=0>Employee</option>
							<option value=1>User</option>
					</select></td>
				</tr>

				<tr>
					<td>Email address</td>
					<td><input type="text" name="email" value="${person.email}"
						required /></td>
				</tr>

				<tr>
					<td>Password</td>
					<td><input type="password" name="password"
						value="${person.password}" required /></td>
				</tr>

				<tr>
					<td>Confirm password</td>
					<td><input type="password" name="confirmpassword"
						value="${person.password}" required /></td>
				</tr>

			</table>

			<h3>Tickets</h3>
			<table>
				<c:forEach items="${person.tickets}" var="ticket">
					<tr>
						<td><c:out value="${ticket.route.name}" /></td>
						<td><c:out value="${ticket.type}" /></td>
						<td><c:out value="${ticket.price}" /></td>
						<td><c:out value="${ticket.creationDate}" /></td>
						<td><c:out value="${ticket.expireDate}" /></td>
					</tr>
				</c:forEach>
			</table>

		</form>
	</div>
</body>
</html>