<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<title>DriveMe</title>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/style.css"/>" rel="stylesheet"
	type="text/css" />

</head>
<body>
	<div id="header">
		<nav>
			<ul class="clearfix">
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/route/ulist">Routes</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/station/ulist">Stations</a></li>
				<li><a
					href="/tick-men-0.0.1-SNAPSHOT/web/ticket/uview/${person.id}">Profile</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/index">Log out</a></li>
			</ul>
		</nav>

	</div>
	<div id="content">

		<form action="../save" method="post">
			<input type="hidden" name="id" value="${station.id}" />


			<table width="360">
				<tr>
					<td>Station</td>
					<td>${station.name}</td>
				</tr>

			</table>

			<br>
			<h3>Routes</h3>
			<br>

			<table width="360">
				<c:forEach items="${station.routes}" var="route">
					<tr>
						<td><c:out value="${route.name}" /></td>
						<td><c:out value="${route.start}" /></td>
						<td><c:out value="${route.end}" /></td>
						<td><c:out value="${route.type}" /></td>
						<td><c:out value="${route.frequency}" /></td>
						<td><c:out value="${route.lengthInKms}" /></td>
						<td><c:out value="${route.lengthInMins}" /></td>
					</tr>

				</c:forEach>
			</table>


		</form>


		<div id="view">
			<a href="/tick-men-0.0.1-SNAPSHOT/web/station/ulist">Back</a>
		</div>
	</div>
</body>
</html>