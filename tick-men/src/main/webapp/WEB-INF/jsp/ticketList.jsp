<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<title>DriveMe</title>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/style.css"/>" rel="stylesheet"
	type="text/css" />

</head>
<body>
	<div id="header">
		<nav>
			<ul class="clearfix">
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/route/list">Routes</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/ticket/list">Tickets</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/station/list">Stations</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/person/list">Users</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/index">Log out</a></li>
			</ul>
		</nav>

	</div>
	<div id="content">
		<table>
			<tr>
				<th>First name</th>
				<th>Last name</th>
				<th>Type</th>
				<th>Price</th>
				<th>Route</th>
				<th>Creation date</th>
				<th>Expire date</th>
			</tr>

			<c:forEach items="${tickets}" var="ticket">
				<tr>
					<td><c:out value="${ticket.person.firstName}" />
					<td><c:out value="${ticket.person.lastName}" />
					<td><c:out value="${ticket.type}" />
					<td><c:out value="${ticket.price}" />
					<td><c:out value="${ticket.route.name}" />
					<td><c:out value="${ticket.creationDate}" />
					<td><c:out value="${ticket.expireDate}" />
					<td><a href="view/${ticket.id}">View</a></td>
				</tr>
			</c:forEach>
		</table>

		<br>
		<div id="view">
			<a href="view/0">New</a>
		</div>
</html>

</div>
</body>
</html>