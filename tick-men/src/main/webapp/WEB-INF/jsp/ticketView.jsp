<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
<title>DriveMe</title>

<link href="<c:url value="/resources/style.css"/>" rel="stylesheet"
	type="text/css" />
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.0/jquery-ui.js"></script>

<script>
	$(function() {
		$("#creationDate").datepicker();
	});
	$(function() {
		$("#expireDate").datepicker();
	});
</script>

</head>
<body>
	<div id="header">
		<nav>
			<ul class="clearfix">
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/route/list">Routes</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/ticket/list">Tickets</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/station/list">Stations</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/person/list">Users</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/index">Log out</a></li>
			</ul>
		</nav>

	</div>
	<div id="content">
		<body>
			<form action="../save" method="post">
				<input type="hidden" name="id" value="${ticket.id}" />


				<table width="360">
					<tr>
						<td>Person</td>
						<td><select name="personId">
								<c:forEach items="${people}" var="person">
									<option value="${person.id}">${person.firstName}
										${person.lastName}</option>
								</c:forEach>
						</select></td>

					</tr>

					<tr>
						<td>Type</td>
						<td><select name="type">
								<option value=0>Normal</option>
								<option value=1>Student</option>
								<option value=2>Pensioner</option>
						</select></td>
					</tr>

					<tr>
						<td>Price</td>
						<td><input type="text" name="price" value="${ticket.price}"
							required /></td>
					</tr>

					<tr>
						<td>Route</td>
						<td><select name="routeId">
								<c:forEach items="${routes}" var="route">
									<option value="${route.id}">${route.name}</option>
								</c:forEach>
						</select></td>
					</tr>

					<tr>
						<td>Creation Date</td>
						<td><input type="text" name="creationDate" id="creationDate"
							value="${ticket.creationDate}" /></td>
					</tr>

					<tr>
						<td>Expire Date</td>
						<td><input type="text" name="expireDate" id="expireDate"
							value="${ticket.expireDate}" /></td>
					</tr>

				</table>


				<input type="submit" value="Save" />

			</form>



			<form action="../delete" method="post">
				<input type="hidden" name="id" value="${ticket.id}" /> <input
					type="submit" value="Delete" />
			</form>
			
			<div id="view">
			<a href="/tick-men-0.0.1-SNAPSHOT/web/ticket/list">Back</a>
		</div>
		</body>
</html>

</div>
</body>
</html>