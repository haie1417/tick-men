<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>DriveMe</title>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/style.css"/>" rel="stylesheet"
	type="text/css" />

</head>
<body>
	<div id="header">
		<nav>
		<ul class="clearfix">
			<li><a href="/tick-men-0.0.1-SNAPSHOT/web/route/list">Routes</a></li>
			<li><a href="/tick-men-0.0.1-SNAPSHOT/web/ticket/list">Tickets</a></li>
			<li><a href="/tick-men-0.0.1-SNAPSHOT/web/station/list">Stations</a></li>
			<li><a href="/tick-men-0.0.1-SNAPSHOT/web/person/list">Users</a></li>
			<li><a href="/tick-men-0.0.1-SNAPSHOT/web/index">Log out</a></li>
		</ul>
		</nav>

	</div>
	<div id="content">
		<table>
			<tr>
				<th>Name</th>
				<th>Start</th>
				<th>End</th>
				<th>Type</th>
				<th>Frequency</th>
				<th>Length(km)</th>
				<th>Time(min)</th>
			</tr>
			<c:forEach items="${routes}" var="route">
				<tr>
					<td><c:out value="${route.name}" /></td>
					<td><c:out value="${route.start}" /></td>
					<td><c:out value="${route.end}" /></td>
					<td><c:out value="${route.type}" /></td>
					<td><c:out value="${route.frequency}" /></td>
					<td><c:out value="${route.lengthInKms}" /></td>
					<td><c:out value="${route.lengthInMins}" /></td>
					<td><a href="view/${route.id}">View</a></td>
				</tr>
			</c:forEach>
		</table>
		<br>
		<div id="view">
			<a href="view/0">New</a>
		</div>
	</div>
</body>
</html>