<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<title>DriveMe</title>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/style.css"/>" rel="stylesheet"
	type="text/css" />

</head>
<body>
	<div id="header">
		<nav>
			<ul class="clearfix">
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/route/list">Routes</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/ticket/list">Tickets</a></li>				
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/station/list">Stations</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/person/list">Users</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/index">Log out</a></li>
			</ul>
		</nav>

	</div>
	<div id="content">

		<form action="../save" method="post">
			<input type="hidden" name="id" value="${station.id}" />


			<table width="360">
				<tr>
					<td>Station</td>
					<td><input type="text" name="name" value="${station.name}" /></td>
				</tr>

				<tr>
					<td>Add Route</td>
					<td><select name="route">

							<option value="0">-- select a route --</option>
							<c:forEach items="${routes}" var="r">
								<option value="${r.id}">${r.name}</option>
							</c:forEach>
					</select></td>
				</tr>

			</table>


			<h2>Routes</h2>
			<table>
				<c:forEach items="${station.routes}" var="route">
					<tr>
						<td><c:out value="${route.name}" /></td>
						<td><c:out value="${route.start}" /></td>
						<td><c:out value="${route.end}" /></td>
						<td><c:out value="${route.type}" /></td>
						<td><c:out value="${route.frequency}" /></td>
						<td><c:out value="${route.lengthInKms}" /></td>
						<td><c:out value="${route.lengthInMins}" /></td>
					</tr>

				</c:forEach>
			</table>


			<input type="submit" value="Save" />

		</form>

		<div id="view">
			<a href="/tick-men-0.0.1-SNAPSHOT/web/station/list">Back</a>
		</div>
	</div>
</body>
</html>