<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

<head>
<title>DriveMe</title>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="<c:url value="/resources/style.css"/>" rel="stylesheet"
	type="text/css" />

</head>
<body>
	<div id="header">
		<nav>
			<ul class="clearfix">
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/route/list">Routes</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/ticket/list">Tickets</a></li>				
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/station/list">Stations</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/person/list">Users</a></li>
				<li><a href="/tick-men-0.0.1-SNAPSHOT/web/index">Log out</a></li>
			</ul>
		</nav>

	</div>
	<div id="content">


		<form action="../save" method="post">
			<input type="hidden" name="id" value="${route.id}" />


			<table width="360">
				<tr>
					<td>Name</td>
					<td><input type="text" name="name" value="${route.name}"
						required /></td>
				</tr>

				<tr>
					<td>Type</td>
					<td><select name="type">
							<option value=0>Bus</option>
							<option value=1>Trolley</option>
							<option value=2>Tram</option>
					</select></td>
				</tr>

				<tr>
					<td>Start</td>
					<td><input type="text" name="start" value="${route.start}"
						required /></td>
				</tr>

				<tr>
					<td>End</td>
					<td><input type="text" name="end" value="${route.end}"
						required /></td>
				</tr>

				<td>Frequency</td>
				<td><input type="text" name="frequency"
					value="${route.frequency}" required /></td>
				</tr>

				<tr>
					<td>Length (km)</td>
					<td><input type="text" name="lengthInKms"
						value="${route.lengthInKms}" required /></td>
				</tr>

				<tr>
					<td>Time (minutes)</td>
					<td><input type="text" name="lengthInMins"
						value="${route.lengthInMins}" required /></td>
				</tr>

				<tr>
					<td>New Stop</td>
					<td><select name="stop">
							<option value="0">-- select a station --</option>
							<c:forEach items="${allStations}" var="station">
								<option value="${station.id}">${station.name}</option>
							</c:forEach>
					</select></td>
				</tr>
			</table>

			<h3>Stops</h3>
			<table>
				<c:forEach items="${route.stations}" var="station">
					<tr>
						<td><c:out value="${station.name}" /></td>
					</tr>
				</c:forEach>
			</table>


			<input type="submit" value="Save" />

		</form>

		<div id="view">
			<a href="/tick-men-0.0.1-SNAPSHOT/web/route/list">Back</a>
		</div>

	</div>
</body>
</html>