package com.fortech.java.services;

import java.util.List;

import com.fortech.java.entities.Route;

public interface RouteService {
	Route get(Long byId);
	List<Route> list();
	void save(Route person);
	void delete(Route person);
}
