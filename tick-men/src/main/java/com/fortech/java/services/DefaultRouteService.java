package com.fortech.java.services;

import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fortech.java.dao.RouteDAO;
import com.fortech.java.entities.Route;

@Service
@Transactional
public class DefaultRouteService implements RouteService {
	@Autowired
	private RouteDAO routeDAO;

	public Route get(Long byId) {
		return routeDAO.get(byId);
	}

	public List<Route> list() {
		return routeDAO.list();
	}

	public void save(Route route) throws ValidationException {
		routeDAO.save(route);
	}

	public void delete(Route route) {
		routeDAO.delete(route);
	}

	public void setRouteDAO(RouteDAO routeDAO) {
		this.routeDAO = routeDAO;
	}
}
