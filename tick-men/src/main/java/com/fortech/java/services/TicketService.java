package com.fortech.java.services;

import java.util.List;

import com.fortech.java.entities.Ticket;

public interface TicketService {
	Ticket get(Long byId);
	List<Ticket> list();
	void save(Ticket ticket);
	void delete(Ticket ticket);
}
