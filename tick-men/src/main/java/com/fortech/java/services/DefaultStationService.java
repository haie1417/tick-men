package com.fortech.java.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fortech.java.dao.StationDAO;
import com.fortech.java.entities.Station;

@Service
@Transactional
public class DefaultStationService implements StationService {
	@Autowired
	private StationDAO stationDAO;

	public Station get(Long byId) {
		return stationDAO.get(byId);
	}

	public List<Station> list() {
		return stationDAO.list();
	}

	public void save(Station station) {
		stationDAO.save(station);
	}

	public void delete(Station station) {
		stationDAO.delete(station);
	}

	public void setStationDAO(StationDAO stationDAO) {
		this.stationDAO = stationDAO;
	}
}
