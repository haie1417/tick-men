package com.fortech.java.services;

import java.util.List;

import com.fortech.java.entities.Station;

public interface StationService {
	Station get(Long byId);

	List<Station> list();

	void save(Station station);

	void delete(Station station);
}
