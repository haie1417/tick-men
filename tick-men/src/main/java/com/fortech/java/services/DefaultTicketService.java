package com.fortech.java.services;

import java.util.List;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fortech.java.dao.TicketDAO;
import com.fortech.java.entities.Ticket;

@Service
@Transactional
public class DefaultTicketService implements TicketService {
	@Autowired
	private TicketDAO ticketDAO;

	public Ticket get(Long byId) {
		return ticketDAO.get(byId);
	}

	public List<Ticket> list() {
		return ticketDAO.list();
	}

	public void save(Ticket ticket) throws ValidationException {
		ticketDAO.save(ticket);
	}

	public void delete(Ticket ticket) {
		ticketDAO.delete(ticket);
	}

	public void setTicketDAO(TicketDAO ticketDAO) {
		this.ticketDAO = ticketDAO;
	}
}
