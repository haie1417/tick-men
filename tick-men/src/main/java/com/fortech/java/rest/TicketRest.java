package com.fortech.java.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fortech.java.entities.Ticket;
import com.fortech.java.services.TicketService;


@Path("/ticket")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Component
public class TicketRest {
	@Autowired(required = true)
	private TicketService ticketService;

	@GET
	@Path("/{byId}")
	@Produces(value = MediaType.APPLICATION_JSON)
	public Response get(@PathParam("byId") Long byId) {
		Ticket ticket = ticketService.get(byId);
		if (ticket != null) {
			return Response.ok(ticket).build();
		} else {
			return Response.noContent().build();
		}
	}

	@GET
	@Path("/list")
	public Response list() {
		return Response.ok(ticketService.list()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response save(Ticket ticket) {
		try {
			ticketService.save(ticket);
			return Response.ok(ticket.getId()).build();
		} catch (Exception ex) {
			return Response.serverError().build();
		}
	}

	@DELETE
	public Response delete(Ticket ticket) {
		try {
			ticketService.delete(ticket);
			return Response.ok().build();
		} catch (Exception ex) {
			return Response.serverError().build();
		}
	}
}
