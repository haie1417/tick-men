package com.fortech.java.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fortech.java.entities.Route;
import com.fortech.java.services.RouteService;


@Path("/route")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Component
public class RouteRest {
	@Autowired(required = true)
	private RouteService routeService;

	@GET
	@Path("/{byId}")
	@Produces(value = MediaType.APPLICATION_JSON)
	public Response get(@PathParam("byId") Long byId) {
		Route route = routeService.get(byId);
		if (route != null) {
			return Response.ok(route).build();
		} else {
			return Response.noContent().build();
		}
	}

	@GET
	@Path("/list")
	public Response list() {
		return Response.ok(routeService.list()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response save(Route route) {
		try {
			routeService.save(route);
			return Response.ok(route.getId()).build();
		} catch (Exception ex) {
			return Response.serverError().build();
		}
	}

	@DELETE
	public Response delete(Route route) {
		try {
			routeService.delete(route);
			return Response.ok().build();
		} catch (Exception ex) {
			return Response.serverError().build();
		}
	}
}
