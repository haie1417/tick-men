package com.fortech.java.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fortech.java.entities.Station;
import com.fortech.java.services.StationService;


@Path("/station")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Component
public class StationRest {
	@Autowired(required = true)
	private StationService stationService;

	@GET
	@Path("/{byId}")
	@Produces(value = MediaType.APPLICATION_JSON)
	public Response get(@PathParam("byId") Long byId) {
		Station station = stationService.get(byId);
		if (station != null) {
			return Response.ok(station).build();
		} else {
			return Response.noContent().build();
		}
	}

	@GET
	@Path("/list")
	public Response list() {
		return Response.ok(stationService.list()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response save(Station station) {
		try {
			stationService.save(station);
			return Response.ok(station.getId()).build();
		} catch (Exception ex) {
			return Response.serverError().build();
		}
	}

	@DELETE
	public Response delete(Station station) {
		try {
			stationService.delete(station);
			return Response.ok().build();
		} catch (Exception ex) {
			return Response.serverError().build();
		}
	}
}
