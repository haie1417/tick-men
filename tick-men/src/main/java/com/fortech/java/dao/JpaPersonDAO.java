package com.fortech.java.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.fortech.java.entities.Person;

public class JpaPersonDAO implements PersonDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public Person get(Long byId) {
		return entityManager.find(Person.class, byId);
	}

	public List<Person> list() {
		return entityManager.createQuery("select r from Person r", Person.class).getResultList();

	}

	public void save(Person drive) {
		entityManager.merge(drive);

	}

	public void delete(Person drive) {
		drive = entityManager.find(Person.class, drive.getId());
		entityManager.remove(drive);

	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
