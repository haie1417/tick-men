package com.fortech.java.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.fortech.java.entities.Station;

public class JpaStationDAO implements StationDAO {
	@PersistenceContext
	private EntityManager entityManager;

	public Station get(Long byId) {
		return entityManager.find(Station.class, byId);
	}

	public List<Station> list() {
		return entityManager.createQuery("select p from Station p", Station.class).getResultList();
	}

	public void save(Station station) {
		entityManager.merge(station);
	}

	public void delete(Station station) {
		station = entityManager.find(Station.class, station.getId());
		entityManager.remove(station);

	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
