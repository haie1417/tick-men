package com.fortech.java.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.fortech.java.entities.Ticket;

public class JpaTicketDAO implements TicketDAO {

	@PersistenceContext
	private EntityManager entityManager;

	public Ticket get(Long byId) {
		return entityManager.find(Ticket.class, byId);
	}

	public List<Ticket> list() {
		return entityManager.createQuery("select r from Ticket r", Ticket.class).getResultList();

	}

	public void save(Ticket drive) {
		entityManager.merge(drive);

	}

	public void delete(Ticket drive) {
		drive = entityManager.find(Ticket.class, drive.getId());
		entityManager.remove(drive);

	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
