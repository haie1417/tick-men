package com.fortech.java.dao;

import java.util.List;

import com.fortech.java.entities.Ticket;


public interface TicketDAO {
	Ticket get(Long byId);
	List<Ticket> list();
	void save(Ticket ticket);
	void delete(Ticket ticket);
}
