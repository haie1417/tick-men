package com.fortech.java.dao;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.fortech.java.entities.Route;
import com.fortech.java.entities.Station;

public class JpaRouteDAO implements RouteDAO {
	
	@PersistenceContext
	private EntityManager entityManager;

	public Route get(Long byId) {
		return entityManager.find(Route.class, byId);
	}

	public List<Route> list() {
		return entityManager.createQuery("select p from Route p", Route.class).getResultList();
	}
	

	public void save(Route route) {
		entityManager.merge(route);
	}

	public void delete(Route route) {
		route = entityManager.find(Route.class, route.getId());
		entityManager.remove(route);
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
