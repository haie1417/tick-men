package com.fortech.java.dao;

import java.util.List;

import com.fortech.java.entities.Route;

public interface RouteDAO {

	Route get(Long byId);
	List<Route> list();
	void save(Route route);
	void delete(Route route);
		
}
