package com.fortech.java.dao;

import java.util.List;

import com.fortech.java.entities.Station;

public interface StationDAO {
	Station get(Long byId);
	List<Station> list();
	void save(Station person);
	void delete(Station person);
}
