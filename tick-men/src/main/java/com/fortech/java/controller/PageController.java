package com.fortech.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fortech.java.entities.Person;
import com.fortech.java.entities.PersonType;
import com.fortech.java.services.PersonService;

@Controller
public class PageController {
	@Autowired
	private PersonService personService;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String indexMapping(Model model) {
		return "index";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginMapping(@RequestParam String email, @RequestParam String password) {
		Person user = new Person();
		user.setEmail(email);
		user.setPassword(password);		
		for (Person p : personService.list()) {
			if (p.equals(user)) {
				user = personService.get(p.getId());
			}
		}
		if (user.getId() != null) {
			if (user.getPersonType().equals(PersonType.USER))
				return "redirect:person/uview/"+user.getId();
			if (user.getPersonType().equals(PersonType.EMPLOYEE))
				return "redirect:route/list";
		}

		return "redirect:index";
	}
}
