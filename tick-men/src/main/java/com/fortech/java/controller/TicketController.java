package com.fortech.java.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fortech.java.entities.Ticket;
import com.fortech.java.entities.TicketType;
import com.fortech.java.services.PersonService;
import com.fortech.java.services.RouteService;
import com.fortech.java.services.TicketService;

@Controller
@RequestMapping("/ticket")
public class TicketController {
	@Autowired
	private TicketService ticketService;
	@Autowired
	private RouteService routeService;
	@Autowired
	private PersonService personService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listTickets(Model model) {
		model.addAttribute("tickets", ticketService.list());
		return "ticketList";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(@RequestParam Long id) {
		Ticket ticket = id != 0 ? ticketService.get(id) : new Ticket();
		ticketService.delete(ticket);
		return "redirect:list";
	}

	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public String getForDay(@PathVariable("id") Long ticketId, Model model) {
		Ticket ticket = ticketId != 0 ? ticketService.get(ticketId) : new Ticket();
		model.addAttribute("ticket", ticket);
		model.addAttribute("routes", routeService.list());
		model.addAttribute("people", personService.list());
		return "ticketView";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@RequestParam Long id, @RequestParam Double price, @RequestParam Integer type,
			@RequestParam Long routeId, @RequestParam Long personId, @RequestParam String creationDate,
			@RequestParam String expireDate) throws ParseException {
		Ticket ticket = new Ticket();
		ticket.setId(id);
		ticket.setPrice(price);

		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		java.util.Date date = format.parse(creationDate);
		ticket.setCreationDate(new Date(date.getTime()));
		date = format.parse(expireDate);
		ticket.setExpireDate(new Date(date.getTime()));

		ticket.setPerson(personService.get(personId));
		ticket.setRoute(routeService.get(routeId));
		switch (type) {
		case 0:
			ticket.setType(TicketType.NORMAL);
			break;
		case 1:
			ticket.setType(TicketType.STUDENT);
			break;
		case 2:
			ticket.setType(TicketType.PENSIONER);
			break;
		}
		ticketService.save(ticket);
		return "redirect:list";
	}
}
