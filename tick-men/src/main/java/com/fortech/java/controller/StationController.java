package com.fortech.java.controller;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fortech.java.entities.Route;
import com.fortech.java.entities.Station;
import com.fortech.java.services.RouteService;
import com.fortech.java.services.StationService;

@Controller
@RequestMapping("/station")
public class StationController {
	@Autowired
	private StationService stationService;
	@Autowired
	private RouteService routeService;

	@RequestMapping(value = "/ulist", method = RequestMethod.GET)
	public String ulistRoutes(Model model) {
		model.addAttribute("stations", stationService.list());
		return "userStationList";
	}

	@RequestMapping(value = "/uview/{id}", method = RequestMethod.GET)
	public String ugetRoute(@PathVariable("id") Long routeId, Model model) {
		Station station = routeId != 0 ? stationService.get(routeId) : new Station();
		model.addAttribute("station", station);
		return "userStationView";
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listStations(Model model) {
		model.addAttribute("stations", stationService.list());
		return "stationList";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(@RequestParam Long id) {
		Station station = id != 0 ? stationService.get(id) : new Station();
		stationService.delete(station);
		return "redirect:list";
	}

	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public String getForDay(@PathVariable("id") Long stationId, Model model) {
		Station station = stationId != 0 ? stationService.get(stationId) : new Station();
		model.addAttribute("station", station);
		model.addAttribute("routes", routeService.list());
		return "stationView";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@RequestParam Long id, @RequestParam String name, @RequestParam Long route) {
		Station station = new Station();
		if (id != null) {
			station = stationService.get(id);
		}
		if (station.getRoutes() == null) {
			station.setRoutes(new HashSet<Route>());
		}
		if (route != 0) {
			station.getRoutes().add(routeService.get(route));
		}
		station.setId(id);
		station.setName(name);
		stationService.save(station);
		if (station.getId() == null) {
			return "redirect:list";
		}
		return "redirect:view/" + station.getId();
	}
}
