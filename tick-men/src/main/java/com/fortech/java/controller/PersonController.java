package com.fortech.java.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fortech.java.entities.Person;
import com.fortech.java.entities.PersonType;
import com.fortech.java.entities.Ticket;
import com.fortech.java.services.PersonService;
import com.fortech.java.services.StationService;
import com.fortech.java.services.TicketService;

@Controller
@RequestMapping("/person")
public class PersonController {
	@Autowired
	private PersonService personService;
	@Autowired
	private StationService stationService;
	@Autowired
	private TicketService ticketService;
	
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listPersons(Model model) {
		model.addAttribute("people", personService.list());
		return "personList";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(@RequestParam Long id) {
		Person person = id != 0 ? personService.get(id) : new Person();
		personService.delete(person);
		return "redirect:list";
	}

	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public String getPerson(@PathVariable("id") Long personId, Model model) {
		Person person = personId != 0 ? personService.get(personId) : new Person();
		Set<Ticket> tickets=new HashSet<Ticket>();
		for (Ticket t:ticketService.list()){
			if (t.getPerson().getId().equals(personId)){
				tickets.add(t);
			}
		}
		person.setTickets(tickets);
		model.addAttribute("person", person);
		model.addAttribute("allStations", stationService.list());
		return "personView";
	}
	
	@RequestMapping(value = "/uview/{id}", method = RequestMethod.GET)
	public String viewPerson(@PathVariable("id") Long personId, Model model) {
		Person person = personId != 0 ? personService.get(personId) : new Person();
		Set<Ticket> tickets=new HashSet<Ticket>();
		for (Ticket t:ticketService.list()){
			if (t.getPerson().getId().equals(personId)){
				tickets.add(t);
			}
		}
		person.setTickets(tickets);
		model.addAttribute("person", person);
		model.addAttribute("allStations", stationService.list());
		return "userPersonView";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@RequestParam Long id, @RequestParam String cnp, @RequestParam String firstName,
			@RequestParam String lastName, @RequestParam String email, @RequestParam String password,
			@RequestParam Integer personType, @RequestParam String action) {

		Person person = new Person();
		if (id != null) {
			person = personService.get(id);
		}

		person.setId(id);
		person.setCnp(cnp);
		person.setFirstName(firstName);
		person.setLastName(lastName);
		person.setEmail(email);
		person.setPassword(password);
		switch (personType) {
		case 0:
			person.setPersonType(PersonType.EMPLOYEE);
			break;
		case 1:
			person.setPersonType(PersonType.USER);
			break;
		}
		personService.save(person);

		if (action.equals("signup"))
			return "redirect:../route/list";
		return "redirect:list";
	}
}
