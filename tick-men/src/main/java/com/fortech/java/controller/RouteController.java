package com.fortech.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fortech.java.entities.Route;
import com.fortech.java.entities.VehicleType;
import com.fortech.java.services.RouteService;
import com.fortech.java.services.StationService;

@Controller
@RequestMapping("/route")
public class RouteController {
	@Autowired
	private RouteService routeService;
	@Autowired
	private StationService stationService;

	@RequestMapping(value = "/ulist", method = RequestMethod.GET)
	public String ulistRoutes(Model model) {
		model.addAttribute("routes", routeService.list());
		return "userRouteList";
	}

	@RequestMapping(value = "/uview/{id}", method = RequestMethod.GET)
	public String ugetRoute(@PathVariable("id") Long routeId, Model model) {
		Route route = routeId != 0 ? routeService.get(routeId) : new Route();
		model.addAttribute("route", route);
		model.addAttribute("allStations", stationService.list());
		return "userRouteView";
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String listRoutes(Model model) {
		model.addAttribute("routes", routeService.list());
		return "routeList";
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String delete(@RequestParam Long id) {
		Route route = id != 0 ? routeService.get(id) : new Route();
		routeService.delete(route);
		return "redirect:list";
	}

	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public String getRoute(@PathVariable("id") Long routeId, Model model) {
		Route route = routeId != 0 ? routeService.get(routeId) : new Route();
		model.addAttribute("route", route);
		model.addAttribute("allStations", stationService.list());
		return "routeView";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@RequestParam Long id, @RequestParam String name, @RequestParam String start,
			@RequestParam String end, @RequestParam Integer lengthInKms, @RequestParam Integer lengthInMins,
			@RequestParam Integer frequency, @RequestParam Integer type, @RequestParam Long stop) {
		Route route = new Route();
		if (id != null) {
			route = routeService.get(id);
		}
		if (stop != 0) {
			route.getStations().add(stationService.get(stop));
		}
		route.setId(id);
		route.setName(name);
		route.setStart(start);
		route.setEnd(end);
		route.setFrequency(frequency);
		route.setLengthInKms(lengthInKms);
		route.setLengthInMins(lengthInMins);
		switch (type) {
		case 0:
			route.setType(VehicleType.BUS);
			break;
		case 1:
			route.setType(VehicleType.TROLLEY);
			break;
		case 2:
			route.setType(VehicleType.TRAM);
			break;
		}
		routeService.save(route);
		if (route.getId() == null) {
			return "redirect:list";
		}
		return "redirect:view/" + route.getId();
	}
}
