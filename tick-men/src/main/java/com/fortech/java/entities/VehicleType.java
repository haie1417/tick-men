package com.fortech.java.entities;

public enum VehicleType {
	BUS(0), TROLLEY(1), TRAM(2);

	private final int value;

	private VehicleType(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
