package com.fortech.java.entities;

public enum TicketType {
	NORMAL(0), STUDENT(1), PENSIONER(2);

	private final int value;

	private TicketType(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
