package com.fortech.java.entities;

public enum PersonType {
	EMPLOYEE(0), USER(1);

	private final int value;

	private PersonType(int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}
}
