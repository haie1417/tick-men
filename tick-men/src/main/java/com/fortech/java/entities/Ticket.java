package com.fortech.java.entities;
/*
 * @Entity
@Inheritance(Strategy=InharitanceType.TABLE_PER_CLASS);
abstract Payments (id,price)
 * */

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.ws.rs.core.Response;

@Entity
public class Ticket {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private double price;
	@Enumerated(EnumType.STRING)
	private TicketType type;

	@ManyToOne
	private Route route;
	@ManyToOne
	private Person person;

	Date creationDate;
	Date expireDate;

	public TicketType getType() {
		return type;
	}

	public Route getRoute() {
		return route;
	}

	public Person getPerson() {
		return person;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setType(TicketType type) {
		this.type = type;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public Ticket(Long id, double price) {
		super();
		this.id = id;
		this.price = price;
	}

	public Ticket() {
		super();
	}

	public Long getId() {
		return id;
	}

	public double getPrice() {
		return price;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + " " + this.id + " " + this.price;
	}
}
