package com.fortech.java.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class Route {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	@Enumerated(EnumType.STRING)
	private VehicleType type;

	private String start;
	private String end;

	private int lengthInKms;
	private int lengthInMins;

	private int frequency;

	@LazyCollection(LazyCollectionOption.FALSE)
	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "route_station", joinColumns = { @JoinColumn(name = "route_id") }, inverseJoinColumns = {
			@JoinColumn(name = "station_id") })
	private Set<Station> stations = new HashSet<Station>(0);

	public Set<Station> getStations() {
		return stations;
	}

	public void setStations(Set<Station> stations) {
		this.stations = stations;
	}

	public VehicleType getType() {
		return type;
	}

	public String getStart() {
		return start;
	}

	public String getEnd() {
		return end;
	}

	public int getLengthInKms() {
		return lengthInKms;
	}

	public int getLengthInMins() {
		return lengthInMins;
	}

	public int getFrequency() {
		return frequency;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setType(VehicleType type) {
		this.type = type;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public void setLengthInKms(int lengthInKms) {
		this.lengthInKms = lengthInKms;
	}

	public void setLengthInMins(int lengthInMins) {
		this.lengthInMins = lengthInMins;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	@Override
	public String toString() {
		return this.id + " " + this.start + " " + this.end + " " + this.frequency + " " + this.lengthInKms + " "
				+ this.lengthInMins + "\nStops:" + this.stations.toString();
	}

}
