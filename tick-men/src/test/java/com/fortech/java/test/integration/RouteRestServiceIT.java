package com.fortech.java.test.integration;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.junit.Assert;
import org.junit.Test;

import com.fortech.java.entities.Route;

public class RouteRestServiceIT {

	private String baseUrl = "http://localhost:8080/drive-me-0.0.1-SNAPSHOT/rest/route/";
	
	@Test
	public void testSave() throws JsonGenerationException,
			JsonMappingException, IOException {
		
		Route route=new Route();
		route.setName("35");
		route.setFrequency(5);
		route.setStart("P-ta garii");
		route.setEnd("Zoriilor");
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonFeature.class);
	
		Client client = ClientBuilder.newClient(clientConfig);
	
		WebTarget webTarget = client
				.target(baseUrl);
	
		Builder request = webTarget.request(MediaType.APPLICATION_JSON);
	
		Response response = request.post(Entity.entity(route, MediaType.APPLICATION_JSON));
		Assert.assertTrue(response.getStatus() == 200);

		Long routeId = response.readEntity(Long.class);
	
		ObjectMapper mapper = new ObjectMapper();
		System.out
				.print("saved *************************** "
						+ mapper.writerWithDefaultPrettyPrinter()
								.writeValueAsString(routeId));
	
	}

	
	@Test
	public void testList() throws JsonGenerationException,
			JsonMappingException, IOException {

		List<Route> list = list();

		ObjectMapper mapper = new ObjectMapper();
		System.out.print(mapper.writerWithDefaultPrettyPrinter()
				.writeValueAsString(list));

		Assert.assertTrue("At least one route is present",
				list != null);
	}

	private List<Route> list() {
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonFeature.class);

		Client client = ClientBuilder.newClient(clientConfig);

		WebTarget webTarget = client
				.target(baseUrl+ "list");

		Builder request = webTarget.request();
		request.header("Content-type", MediaType.APPLICATION_JSON);

		Response response = request.get();
		Assert.assertTrue(response.getStatus() == 200);

		List<Route> podcasts = response
				.readEntity(new GenericType<List<Route>>() {
				});
		return podcasts;
	}

	@Test
	public void testGetById() throws JsonGenerationException,
			JsonMappingException, IOException {

		List<Route> list = list();
		if (list.size() == 0){
			return;
		}
		Long routeId = list.get(0).getId();
		
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonFeature.class);

		Client client = ClientBuilder.newClient(clientConfig);

		WebTarget webTarget = client
				.target(baseUrl + routeId);

		Builder request = webTarget.request(MediaType.APPLICATION_JSON);

		Response response = request.get();
		Assert.assertTrue(response.getStatus() == 200);

		Route podcast = response.readEntity(Route.class);

		ObjectMapper mapper = new ObjectMapper();
		System.out
				.print("Received from database *************************** "
						+ mapper.writerWithDefaultPrettyPrinter()
								.writeValueAsString(podcast));

	}
	
	
	
}
