package com.fortech.java.test.integration;

import java.io.IOException;
import java.util.List;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.junit.Assert;
import org.junit.Test;

import com.fortech.java.entities.Ticket;
import com.fortech.java.entities.TicketType;

public class TicketRestServiceIT {

	private String baseUrl = "http://localhost:8080/drive-me-0.0.1-SNAPSHOT/rest/ticket/";
	
	@Test
	public void testSave() throws JsonGenerationException,
			JsonMappingException, IOException {
		
		Ticket ticket=new Ticket();
		ticket.setType(TicketType.NORMAL);
		ticket.setPrice(110);
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonFeature.class);
	
		Client client = ClientBuilder.newClient(clientConfig);
	
		WebTarget webTarget = client
				.target(baseUrl);
	
		Builder request = webTarget.request(MediaType.APPLICATION_JSON);
	
		Response response = request.post(Entity.entity(ticket, MediaType.APPLICATION_JSON));
		Assert.assertTrue(response.getStatus() == 200);
		
		
		Long ticketId = response.readEntity(Long.class);
	
		ObjectMapper mapper = new ObjectMapper();
		System.out
				.print("saved *************************** "
						+ mapper.writerWithDefaultPrettyPrinter()
								.writeValueAsString(ticketId));
	
	}

	
	@Test
	public void testList() throws JsonGenerationException,
			JsonMappingException, IOException {

		List<Ticket> list = list();

		ObjectMapper mapper = new ObjectMapper();
		System.out.print(mapper.writerWithDefaultPrettyPrinter()
				.writeValueAsString(list));

		Assert.assertTrue("At least one ticket is present",
				list != null);
	}

	private List<Ticket> list() {
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonFeature.class);

		Client client = ClientBuilder.newClient(clientConfig);

		WebTarget webTarget = client
				.target(baseUrl+ "list");

		Builder request = webTarget.request();
		request.header("Content-type", MediaType.APPLICATION_JSON);

		Response response = request.get();
		Assert.assertTrue(response.getStatus() == 200);

		List<Ticket> podcasts = response
				.readEntity(new GenericType<List<Ticket>>() {
				});
		return podcasts;
	}

	@Test
	public void testGetById() throws JsonGenerationException,
			JsonMappingException, IOException {

		List<Ticket> list = list();
		if (list.size() == 0){
			return;
		}
		Long ticketId = list.get(0).getId();
		
		
		ClientConfig clientConfig = new ClientConfig();
		clientConfig.register(JacksonFeature.class);

		Client client = ClientBuilder.newClient(clientConfig);

		WebTarget webTarget = client
				.target(baseUrl + ticketId);

		Builder request = webTarget.request(MediaType.APPLICATION_JSON);

		Response response = request.get();
		Assert.assertTrue(response.getStatus() == 200);

		Ticket podcast = response.readEntity(Ticket.class);

		ObjectMapper mapper = new ObjectMapper();
		System.out
				.print("Received from database *************************** "
						+ mapper.writerWithDefaultPrettyPrinter()
								.writeValueAsString(podcast));

	}
	
	
	
}
